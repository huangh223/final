#if !defined(DFT_H)
#define      DFT_H

#include <stddef.h>
#include <mpi.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct _dft *DFT;

/** Each process/device gets a 3D block of the indices.  N is the number of points per dimension
 * dimension.  So each of the *Start and *End pairs should be a subinterval
 * of [0, N], and indicates that this block goes from coordinates
 * [*Start, * ...., *End - 1] in that direction.

 The indexing is in lexicographic ordering, so the global index of (i, j, k)
 is

 i + N * (j + N * k),

 whereas the *local* index of the same (i, j, k) is

 i - ib.xStart + (ib.xEnd - ib.xStart) * (j - ib.yStart + (ib.yEnd - yStart) * (k - ib.zStart)

 You are responsible for understanding how the to convert between local and
 global indexes for your choice of partition.
 */
struct IndexBlock
{
  size_t xStart, xEnd;
  size_t yStart, yEnd;
  size_t zStart, zEnd;
};

/** Create a discrete Fourier transform context object
 *
 * This object encapsulates all the decisions about how best to implement DFT
 * for various number of processors and problem sizes
 *
 * comm: input, MPI communicator describing the parallel layout.
 * out:  dft_p, the object that will define the graph on which we will
 * search.
 */
int DFTCreate(MPI_Comm comm, DFT *dft_p);
/** Destroy the object */
int DFTDestroy(DFT *dft_p);

/** Get arrays for the fields to be filled.

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension), a power of 2
  \param[out]    iBlock        A description of this MPI process's block
  \param[out]    xLocal        This should be made to point to an array, allocated by you, where you would like the local physical space vector to be stored
  \param[out]    yLocal        This should be made to point to an array, allocated by you, where you would like the local frequency space vector to be stored
*/
int DFTGetFieldArrays(DFT dft, size_t N, struct IndexBlock *iBlock, double _Complex **xLocal_p, double _Complex **yLocal_p);

/** Restore arrays for the fields.

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension), a power of 2
  \param[in]     iBlock        A description of this MPI process's block
  \param[in/out] xLocal        This should free the array allocated by you
  \param[in/out] yLocal        This should free the array allocated by you
*/
int DFTRestoreFieldArrays(DFT dft, size_t N, struct IndexBlock *iBlock, double _Complex **xLocal_p, double _Complex **yLocal_p);

/** Apply the discrete Fourier transform

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension), a power of 2
  \param[in]     iBlock        A description of this MPI process's block
  \param[in]     xLocal        physical space array, local portion described by iBlock
  \param[out]    yLocal        frequency space, discrete Fourier transform of x, local portion described by iBlock.
*/
int DFTTransform(DFT dft, size_t N, struct IndexBlock *iBlock, const double _Complex *xLocal, double _Complex *yLocal);

#if defined(__cplusplus)
}
#endif
#endif
