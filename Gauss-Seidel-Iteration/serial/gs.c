#include "gs.h"

int gauss_seidel_iteration(size_t n, int n_iter, const double *f, double *u)
{
  return 0;
}

int gauss_seidel_ordering(size_t n, size_t *order)
{
  for (size_t i = 0; i < (n - 1) * (n - 1) * (n - 1); i++) {
    order[i] = i;
  }

  return 0;
}
