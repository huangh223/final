#if !defined(GS_H)
#define      GS_H

#include <stddef.h>
#include <mpi.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct _gs *GS;

/** Each process/device gets a 3D block of the indices.  N is the number of
 * cells per dimension, so N - 1 is the number of degrees of freedom per
 * dimension.  So each of the *Start and *End pairs should be a subinterval
 * of [0, N - 1], and indicates that this block goes from coordinates
 * [*Start, * ...., *End - 1] in that direction.

 The indexing is in lexicographic ordering, so the global index of (i, j, k)
 is

 i + (N - 1) * (j + (N - 1) * k),

 whereas the *local* index of the same (i, j, k) is

 i - ib.xStart + (ib.xEnd - ib.xStart) * (j - ib.yStart + (ib.yEnd - yStart) * (k - ib.zStart)

 You are responsible for understanding how the to convert between local and
 global indexes for your choice of partition.
 */
struct IndexBlock
{
  size_t xStart, xEnd;
  size_t yStart, yEnd;
  size_t zStart, zEnd;
};

/** Create a Gauss-Seidel iteration context object
 *
 * This object encapsulates all the decisions about how best to implement GS
 * for various number of processors and problem sizes
 *
 * comm: input, MPI communicator describing the parallel layout.
 * out:  gs_p, the object that will define the graph on which we will
 * search.
 */
int GSCreate(MPI_Comm comm, GS *gs_p);
/** Destroy the object */
int GSDestroy(GS *gs_p);

/** Get arrays for the fields to be filled.

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension)
  \param[out]    iBlock        A description of this MPI process's block
  \param[out]    fLocal        This should be made to point to an array, allocated by you, where you would like the local RHS vector to be stored
  \param[out]    uLocal        This should be made to point to an array, allocated by you, where you would like the local solution vector to be stored
*/
int GSGetFieldArrays(GS gs, size_t N, struct IndexBlock *iBlock, double **fLocal, double **uLocal);

/** Restore the allocated field arrays.

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension)
  \param[in]     iBlock        A description of this MPI process's block
  \param[in/out] fLocal        You should dellocate this array that you created above
  \param[in/out] uLocal        You should dellocate this array that you created above
*/
int GSRestoreFieldArrays(GS gs, size_t N, struct IndexBlock *iBlock, double **fLocal, double **uLocal);

/** Gauss-Seidel iteration on a 3D unit cube discretization of the Poisson
 *  equation $-\Delta u = f$ with homogeneous Dirichlet boundary conditions,
 *  using a 21-point stencil:
 *
 *        8
 *       -- : stencil center
 *       3N
 *
 *       -1
 *       -- : stencil edge neighbors
 *       6N
 *
 *       -1
 *      --- : stencil vertex neighbors
 *      12N

  \param[in]     N         The number of cells per dimension (one more than the
                           number of degrees of freedom per dimension)
  \param[in]     n_iter    The number of Gauss-Seidel iterations to perform.
  \param[in]     iBlock    The block describing the local portion of the vectors.
  \param[in]     f         [size determined by iBlock] The local portion of the right hand side.
  \param[in/out] u         [size determined by iBlock] The local portion of the solution.

  Note: you may reorder the degrees of freedom, and you may use your own relaxation constant.
 */
int GSIterate(GS gs, size_t N, int n_iter, struct IndexBlock *iBlock, const double *fLocal, double *uLocal);

/** Get arrays for the fields to be filled.

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension)
  \param[in]     numDevices    The number of cuda devices available
  \param[out]    iBlock        A description of the index block for each cuda device
  \param[out]    fLocal        For each cuda device, this should be made to
                               point to an array, allocated by you *on the device*, where you would like
                               the local RHS vector to be stored (can be NULL if the device is empty)
  \param[out]    uLocal        For each cuda device, this should be made to
                               point to an array, allocated by you *on the device*,
                               where you would like the local solution vector to be stored (can be NULL if the device is empty)
*/
int GSGetFieldArraysDevice(GS gs, size_t N, int numDevices, struct IndexBlock iBlock[], double *fLocal[], double *uLocal[]);

/** Restore arrays for the fields to be filled.

  \param[in]     N             The number of cells per dimension (one greather than the number of degrees of freedom per dimension)
  \param[in]     numDevices    The number of cuda devices available
  \param[in]     iBlock        A description of the index block for each cuda device
  \param[in/out] fLocal        The array of device pointers that you allocated and should deallocate.
  \param[in/out] uLocal        The array of device pointers that you allocated and should deallocate.
*/
int GSRestoreFieldArraysDevice(GS gs, size_t N, int numDevices, struct IndexBlock iBlock[], double *fLocal[], double *uLocal[]);

/** Gauss-Seidel iteration on a 3D unit cube discretization of the Poisson
 *  equation $-\Delta u = f$ with homogeneous Dirichlet boundary conditions,
 *  using a 21-point stencil:
 *
 *        8
 *       -- : stencil center
 *       3N
 *
 *       -1
 *       -- : stencil edge neighbors
 *       6N
 *
 *       -1
 *      --- : stencil vertex neighbors
 *      12N

  \param[in]     N           The number of cells per dimension (one more than the
                             number of degrees of freedom per dimension)
  \param[in]     n_iter      The number of Gauss-Seidel iterations to perform.
  \param[in]     numDevices  The number of cuda devices,
  \param[in]     iBlock      The block describing the portion of the vectors on each cuda device.
  \param[in]     f           [sizes determined by iBlock] The portion of the right hand side for each device.
  \param[in/out] u           [sizes determined by iBlock] The portion of the solution for each device.

  Note: you may reorder the degrees of freedom, and you may use your own relaxation constant.
 */
int GSIterateDevice(GS gs, size_t N, int num_iter,  int numDevices, const struct IndexBlock iBlock[], const double *fLocal[], double *uLocal[]);

#if defined(__cplusplus)
}
#endif
#endif
