# Final Projects

**Details of the checkpoints may be revised until they are assigned on T-Square.**

There are five problems to choose as project topics:

* Two problems with dense calculations:

    * [Discrete Fourier transform](Discrete-Fourier-Transform/)
    * [Matrix-matrix multiply-add](Matrix-Matrix-Multiply-Add/)

* Two problems with structured calculations:

    * [Lattice Boltzmann method](Lattice-Boltzmann-Method/)
    * [Gauss-Seidel iteration](Gauss-Seidel-Iteration/)

* One problem with unstructured calculations:

    * [Breadth-first search](Breadth-First-Search/)

## Proper Conduct 

* Teams may discuss their work with each other.
* Algorithms / results that you did not derive must be cited.
* All writing and code must be original:
    * External libraries like BLAS are prohibited without special permission.

## Checkpoint 0: Choose Your Project & Team

**Due: Friday, September 8**

* You may work in pairs if you'd like.

## Checkpoint 1: Serial Analysis & Implementation

**Due: Friday, September 29**

* Submission will be via repository transfer on bitbucket, submitted by 11:59:59 p.m.
* Teams name your repository with both gtusernames: `cse6230fa17-final-gtuser1-gtuser2`.

Report (5 points):

* PDF in the `serial` directory of your chosen topic.
* Include citations where relevant.
* Give some examples of scientific/enginering/real-world applications to which your kernel is relevant (1 points).
* Give asymptotic serial analysis (RAM model) (4 points):
    * What is the best asymptotic performance ("Big-O") of existing serial algorithms?
    * What is the theoretical best lower bound on asymptotic performance, if it is different?
    * What is the asymptotic performance of your code?
    * To the best of your ability, characterize the number of arithmetic and logical
      operations (additions/subtractions, multiplications, divisions, comparisons, etc.) of your code as a function of the size parameters of your problem.
        * i.e., if possible, give a non-asymptotic, concrete expression, like a polynomial.
    * To the best of your ability, characterize the space requirements in bytes of your code as a function of the size parameters of your problem (include temporary workspace)
        * i.e., if possible, give a non-asymptotic, concrete expression, like a polynomial.
   

Code (5 points):

* Write a valid serial code that performs your calculation.
* It must conform to an API that we will specify for each project choice in the `serial` directory.
    * Do not change the Makefile, the header files, or the test file
* We will test your code on deepthought much like project 1, but...
    * This time your code is structured as a *library*:
        * The test program know none of the details, only links against the library and calls the functions.
        * The test program depends on **PETSc**:
            * Modules available on deepthought: `module use /nethome/tisaac3/opt/deepthought/modulefiles` and `module load petsc/cse6230-single` or `module load petsc/cse6230-double`
            * On your own computer: follow instructions on [the PETSc website](http://www.mcs.anl.gov/petsc) (if you are doing the DFT project, configure with `--download-fftw` option)
    * Points deducted for code that is hard to follow (-2), does not run out of the box on deepthought (-1), does not pass (-3)
       

## Checkpoint 2: Parallel Analysis & Machine Choice

**Due: Friday, November 17**

* Submission will be via repository transfer on bitbucket, submitted by 11:59:59 p.m.
* Use the same repository as for Checkpoint 1: pull in changes from the master `final` repo when they are posted.

Report (10 points):

* Choose a computing platform for your parallel implementation:
    * Stampede2, Intel Xeon Phi KNL: the final checkpoint will be run on multiple nodes.
    * Bridges, K80 (1--4) or P100 (1--2): you may choose to use a single or multiple devices on a single node.
* Give pseudocode for your algorithm in a parallel machine model that is relevant to your choice of platform.
    * You are allowed to "call" the serial version of your algorithm within your parallel algorithm without redoing the pseudocode for the serial call.
    * If you choose an algorithm found elsewhere, cite it.
* Discuss the characteristics of the machine that will be most relevant to the performance of your code, such as bandwidth and FLOP/s.
* Give asymptotic analysis of your pseudocode include the problem size parameters of your project ("N") as well as the degree of parallelism ("P").
* Given the characteristics of your algorithm and your machine, what do you
  expect to be the bottleneck to performance?  Will that bottleneck depend on,
  "N", "P" or the ratio "N:P"?  Justify your answer.

## Checkpoint 3: Parallel Implementation

**Due: Friday, November 17**

* Submission will be via repository transfer on bitbucket, submitted by 11:59:59 p.m.
* Use the same repository as for Checkpoint 1: pull in changes from the master `final` repo when they are posted.

Code (10 points):

* Write a valid parallel code that performs your calculation.
* It must conform to the API for your project.
    * If you are using the GPUs, add a `Makefile.cuda` in the `parallel` directory for your project with the line `{MMMA,BFS,GS,DFT,LBM}_CUDA = 1`.
    * On bridges `module use /home/tisaac/opt/modulefiles`; on stampede2 `module use /home1/01236/tisaac/opt/modulefiles`.  Then `module load petsc/cse6230-{single,double}`
* We will have a driver for running each code: your primary goal is correctness.
    * To help us automate the correctness tests, please add the following files in your `[Project-Type]/parallel/` directory:
        * `.stampede2` if your code should be tested on stampde2
        * `.k80` if your code should be tested on bridges with the k80 devices
        * `.p100` if your code should be tested on bridges with the p100 devices
        * [MMMA only] `.single` if your code should be tested in single precision
        * [MMMA only] `.double` if your code should be tested in single precision
* Verify that instructors/graders can run the driver on your platform.

## Final: Parallel Optimization

**Due: Thursday, December 14 at 5 p.m.  No extensions; no exceptions.**

* Submission will be via repository transfer on bitbucket, submitted by 5:00 p.m.
* Use the same repository as for Checkpoint 1, 2 & 3: pull in changes from the master `final` repo when they are posted.

Code (6 points):

* Submit your best performing code.
    * Keep the dot files from before.  For the final submission, you may choose your final batch submission script.
    * You should name your final submission script `slurm-final.sh`.  We should be able to check out your code on the
      machine of your choice an run it.  As usual, points will be deducted if it doesn't run out of the box.
    * You can change the amount of parallelism (number of nodes / devices, number of processes per node), and
      you can change the scale of the problem (all tests take a `-tests`
      parameter that specifies that you want to run 4 tests of varying size).
      If your code doesn't scale up to the largest size, `slurm-final.sh` should include tests of the largest tests
      your code can complete.
  
Notebook (6 points):

* Entries as in Project 2 & Exercise 6:
  * A script that shows what you tested
  * Justification via profiling or analysis of what you think needs to be changed
  * Proposed changes
  * Follow up testing with the same script to see if the expected improvements
* At least 3 entries: no points deducted for unsuccessful optimization attempts,
  *if* you discuss *why* you think it was unsuccessful: what was wrong with
  your expectations?  Did you learn something new about the algorithm/performance of the machine?
* **One extra credit point (up to 3)** for *meaningful* (my discretion) notebook entries beyond 3

Final Analysis (6 points):

* 0.5 -- 1 page
* Compare the final performance of your code to the predictions you made in Checkpoint 2:
  * Are the bottlenecks as expected?  If they differ, what was wrong with your initial performance model?
  * Do the performance parameters (Bandwidths, flop rates) seem to differ significantly from
    your prediction / the reported values ?  If so, why?
* If there are optimizations you would like to make but don't have time for,
  explain why you think they would work here.
